﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_Project1
{
    public class Student
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string FullName
        {
            get { return $"{Surname} {Name}"; }
        }
        public string Email { get; set; }
        public byte Age { get; set; }

        public static Student[] CreateStudent(int n)
        {
            Random rand = new Random();
            var students = new Student[n];
            for (int i = 0; i < students.Length; i++)
            {
                var student = new Student();
                student.Name = $"A{i + 1}";
                student.Surname = $"A{i + 1}yan";
                student.Email = $"A{i + 1}$gmail.com";
                student.Age = (byte)rand.Next(15, 60);
                students[i] = student;
            }
            return students;
        }

        public static void Shuffle(Student[] stud)
        {
            Random random = new Random();
            for (int i = stud.Length - 1; i > 0; i--)
            {
                int swapIndex = random.Next(i + 1);
                Swap(stud, i, swapIndex);
            }
        }

        private static void Swap(Student[] stud, int i, int index)
        {
            Student temp = stud[i];
            stud[i] = stud[index];
            stud[index] = temp;
        }
    }
}
