﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_Project1
{
    public class MicMeneger
    {
        public static void MenegerGroup(Student[] students, Teacher[] teachars)
        {
            int counter = 0;
            string docNo = "";
            int index = 0;
            Group gr = new Group();
           
            Teacher teacher = new Teacher();
            for (int i = 0; i < teachars.Length; i++)
            {
                List<Student> sublist = SublistStudents(students, teachars, i, counter);
                List<Student> studentGroup = sublist;
                teacher.Name = teachars[i].Name;
                index = i + 1;
                docNo = "Group No:" + index.ToString();
                gr.Name = $"{teachars[i].Subject}{" "}{docNo}";
                Console.WriteLine();
                Console.WriteLine($"{gr.Name}{"(TEACHER:"}{teachars[i].Name}{")"}");
                Print(studentGroup);
                counter++;
            }          
        }

        private static List<Student> SublistStudents(Student[] stud, Teacher[] teachars, int i, int counter)
        {
            int step = 0;
            int mnacord = stud.Length % teachars.Length;
            List<Student> sublist = new List<Student>();

            int s = i * (stud.Length / teachars.Length);
            if (counter == teachars.Length - 1)
                step = i * (stud.Length / teachars.Length) + mnacord;
            else
                step = s;
         
            for (int l = s; l < stud.Length / teachars.Length + step; l++)
            {
                sublist.Add(stud[l]);
            }

            return sublist;
        }

        static void Print(List<Student> students)
        {
            for (int i = 0; i < students.Count; i++)
            {
                if (students[i] != null)
                    Console.WriteLine($"{i+1}{"."}{students[i].FullName} {students[i].Age}{students[i].Email}");
                else
                    Console.WriteLine();
            }
        }
    }
}
