﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_Project1
{
    class Program
    {
        static void Main(string[] args)
        {
            Student[] students = Student.CreateStudent(38);
            //Print(students);

            Student.Shuffle(students);
            //Print(students);

            Teacher[] teachers = Teacher.CreateTeacher(3);
            //Print(teachers);
            MicMeneger gr = new MicMeneger();
            MicMeneger.MenegerGroup(students, teachers);

            Console.ReadLine();
        }

        static void Print(Teacher[] teachers)
        {
            for (int i = 0; i < teachers.Length; i++)
            {
                if (teachers[i] != null)
                    Console.WriteLine($"{teachers[i].FullName} {teachers[i].Subject}");
                else
                    Console.WriteLine();
            }
        }

        static void Print(Student[] students)
        {
            for (int i = 0; i < students.Length; i++)
            {
                if (students[i] != null)
                    Console.WriteLine($"{students[i].FullName} {students[i].Age}");
                else
                    Console.WriteLine();
            }
        }
    }
}
