﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_Project1
{
    public class Teacher
    {
        public string Name { get; set; }
        public string Surname { get; set; }

        public string FullName
        {
            get { return $"{Surname} {Name}"; }
        }

        public string Email { get; set; }
        public int Salary { get; set; }
        public string Subject
        {
            get { return "Programming with C#"; }
        }

        public static Teacher[] CreateTeacher(int n)
        {
            Random rand = new Random();
            var teachers = new Teacher[n];
            for (int i = 0; i < teachers.Length; i++)
            {
                var teacher = new Teacher();
                teacher.Name = $"B{i + 1}";
                teacher.Surname = $"B{i + 1}yan";
                teacher.Email = $"B{i + 1}$gmail.com";
                teacher.Salary = rand.Next(300000, 600000);
                teachers[i] = teacher;
            }
            return teachers;
        }

    }
}
